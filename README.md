Julia is a high-level, high-performance, dynamic programming language. While it is a general-purpose language and can be used to write any application, many of its features are well suited for numerical analysis and computational science.

**Install Julia **

`wget https://julialang-s3.julialang.org/bin/linux/x64/1.6/julia-1.6.1-linux-x86_64.tar.gz`
`tar zxvf julia-1.6.1-linux-x86_64.tar.gz`

`sudo apt install julia`

